#include "../src/utils.cpp"
#include "../src/item.cpp"
#include <cassert>

int main()
{
    string str1 = "1 imported bottle of perfume at 27.99";
    Item item1 = parseItem(str1);
    assert(item1.quantity == 1);
    assert(item1.imported == true);
    assert(item1.exempt == false);
    assert(item1.price == 27.99);

    string str2 = "3 chocolate bars at 0.85";
    Item item2 = parseItem(str2);
    assert(item2.quantity == 3);
    assert(item2.imported == false);
    assert(item2.exempt == true);
    assert(item2.price == 0.85);

    string str3 = "1 book at 12.49";
    Item item3 = parseItem(str3);
    assert(item3.quantity == 1);
    assert(item3.imported == false);
    assert(item3.exempt == true);
    assert(item3.price == 12.49);

    cout << "All tests passed!" << endl;

    return 0;
}
