#include <cassert>
#include "../src/basket.cpp"

int main()
{
    Basket basket;
    assert(basket.total == 0);
    assert(basket.taxes == 0);

    Item item1("book", 1, false, true, 12.49);
    Item item2("music CD", 1, false, false, 14.99);
    Item item3("chocolate bar", 1, false, true, 0.85);

    basket.addItem(item1);
    basket.addItem(item2);
    basket.addItem(item3);

    assert(basket.items.size() == 3);
    assert(abs(basket.taxes - 1.50) < 0.001);
    assert(abs(basket.total - 29.83) < 0.001);

    basket.items.clear();
    basket.total = 0;
    basket.taxes = 0;

    Item item4("imported box of chocolates", 1, true, true, 10.00);
    Item item5("imported bottle of perfume", 1, true, false, 47.50);

    basket.addItem(item4);
    basket.addItem(item5);

    assert(basket.items.size() == 2);
    assert(abs(basket.taxes - 7.65) < 0.001);
    assert(abs(basket.total - 65.15) < 0.001);

    basket.items.clear();
    basket.total = 0;
    basket.taxes = 0;

    Item item6("imported bottle of perfume", 1, true, false, 27.99);
    Item item7("bottle of perfume", 1, false, false, 18.99);
    Item item8("packet of headache pills", 1, false, true, 9.75);
    Item item9("box of imported chocolates", 1, true, true, 11.25);

    basket.addItem(item6);
    basket.addItem(item7);
    basket.addItem(item8);
    basket.addItem(item9);

    assert(basket.items.size() == 4);
    assert(abs(basket.taxes - 6.70) < 0.001);
    assert(abs(basket.total - 74.68) < 0.001);

    basket.items.clear();
    basket.total = 0;
    basket.taxes = 0;

    cout << "All tests passed!" << endl;

    return 0;
}
