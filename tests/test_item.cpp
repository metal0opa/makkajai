#include <cassert>
#include "../src/item.cpp"

void test_item()
{
    // Test that the tax is calculated correctly for a non-exempt, non-imported item
    Item item1("perfume", 1, false, false, 18.99);
    assert(abs(item1.tax - 1.90) < 0.0001);
    // Test that the tax is calculated correctly for an imported, non-exempt item
    Item item2("imported chocolate", 1, true, false, 4.99);
    assert(abs(item2.tax - 0.75) < 0.0001);

    // Test that the tax is calculated correctly for a non-imported, exempt item
    Item item3("book", 2, false, true, 12.49);
    assert(abs(item3.tax - 0.0) < 0.0001);

    // Test that the price with tax is calculated correctly
    Item item4("imported perfume", 1, true, false, 47.50);
    assert(abs(item4.getPriceWithTax() - 54.65) < 0.0001);

    cout << "All tests passed!" << endl;
}

int main()
{
    test_item();
    return 0;
}
