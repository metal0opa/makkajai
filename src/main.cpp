#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include <cmath>

#include "basket.cpp"

using namespace std;

int main()
{
    cout << "Enter items in the following format: <quantity> <name> at <price>" << endl;
    cout << "Enter an empty line to exit." << endl
         << endl;

    Basket basket;
    string str;
    while (getline(cin, str))
    {
        if (str.empty())
            break;
        basket.addItem(parseItem(str));
    }

    cout << basket.generateReceipt();

    return 0;
}
