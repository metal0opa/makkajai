#include <iomanip>

#include "utils.cpp"

using namespace std;

Item::Item(string name, int quantity, bool imported, bool exempt, double price)
    : name(name), quantity(quantity), imported(imported), exempt(exempt), price(price * quantity)
{
    // Calculate tax based on item properties
    double rate = 0.0;
    if (!exempt)
        rate += 0.1;
    if (imported)
        rate += 0.05;
    tax = ceil(rate * price * 20) / 20.0;

    tax *= quantity; // Multiply tax by quantity
}

double Item::getPriceWithTax()
{
    return price + tax;
}

string Item::description()
{

    string description = to_string(quantity) + " " + name + ": " + to_string_with_precision(getPriceWithTax());
    return description;
}