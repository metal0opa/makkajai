#ifndef BASKET_H
#define BASKET_H

#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

#include "item.cpp"

using namespace std;
// Class representing a shopping basket
class Basket
{
public:
    vector<Item> items;
    double total = 0;
    double taxes = 0;

    // Add item to basket
    void addItem(Item item);

    // Generate receipt
    string generateReceipt();
};

#endif