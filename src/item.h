#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <iostream>
#include <cmath>

using namespace std;

// Class representing a single item
class Item
{
public:
    string name;
    int quantity;
    bool imported;
    bool exempt;
    double price;
    double tax;

    // Constructor - takes in item properties and calculates tax
    Item(string name, int quantity, bool imported, bool exempt, double price);

    // Get price with tax
    double getPriceWithTax();

    // Generate a string representation of the item
    string description();
};

#endif