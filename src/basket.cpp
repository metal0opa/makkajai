#include "basket.h"

using namespace std;

void Basket::addItem(Item item)
{
    items.push_back(item);
    total += item.getPriceWithTax();
    taxes += item.tax;
}

string Basket::generateReceipt()
{
    string receipt = "";
    for (auto &item : items)
    {
        receipt += item.description() + "\n";
    }
    // double values are rounded to 2 decimal places
    receipt += "Sales Taxes: " + to_string_with_precision(taxes) + "\n";
    receipt += "Total: " + to_string_with_precision(total) + "\n\n";

    return receipt;
}
