#include "item.h"
using namespace std;

// Function to check if input is valid
bool isValidInput(const string &str, size_t at_pos)
{
    size_t space_pos = str.find(" ");
    if (!isdigit(str[space_pos - 1]))
        throw invalid_argument("Invalid input: Quantity not found");

    if (at_pos == string::npos)
        throw invalid_argument("Invalid input: Price not found");
    return true;
}

// Function to parse an item from a string
Item parseItem(const string &str)
{
    string exemptWords[] = {"book", "food", "chocolate", "pill", "banana", "apple"};
    size_t at_pos = str.find(" at ");

    isValidInput(str, at_pos);

    string name = str.substr(2, at_pos - 2);
    int quantity = stoi(str.substr(0, str.find(" ")));
    double price = stod(str.substr(at_pos + 4));
    bool imported = name.find("imported") != string::npos;
    bool exempt = false;
    for (const auto &word : exemptWords)
        if (name.find(word) != string::npos)
        {
            exempt = true;
            break;
        }

    return Item(name, quantity, imported, exempt, price);
}

// function to convert a double to a string with 2 decimal places
string to_string_with_precision(const double a_value, const int n = 2)
{
    ostringstream out;
    out << fixed << setprecision(n) << a_value;
    return out.str();
}